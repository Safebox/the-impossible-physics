package org.tiph.constants;

import org.tiph.units.base.Kelvin;
import org.tiph.units.base.Kilogram;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;
import org.tiph.units.derived.*;

public class Derived
{
    public static GenericDerived VACUUM_MAGNETIC_PERMEABILITY = new GenericDerived(4.0 * Math.PI * 1E-7, "μ_0", "N A^-2");
    public static GenericDerived VACUUM_ELECTRIC_PERMITTIVITY = new GenericDerived(1.0 / (VACUUM_MAGNETIC_PERMEABILITY.getValue() * Math.pow(Defined.SPEED_OF_LIGHT_IN_VACUUM.getValue(), 2.0)), "ε_0", "F m^-1");
    public static Ohm CHARACTERISTIC_IMPEDANCE_OF_VACUUM = new Ohm(VACUUM_MAGNETIC_PERMEABILITY.getValue() * Defined.SPEED_OF_LIGHT_IN_VACUUM.getValue(), "Z_0");
    public static GenericDerived NEWTONIAN_CONSTANT_OF_GRAVITATION = new GenericDerived(6.67430E-11, "G", "m^3 kg^-1 s^-2");
    public static GenericDerived REDUCED_PLANCK_CONSTANT = new GenericDerived(Defined.PLANCK_CONSTANT.getValue() / (2.0 * Math.PI), "ħ", "J s");
    public static Kilogram PLANCK_MASS = new Kilogram(Math.pow((REDUCED_PLANCK_CONSTANT.getValue() * Defined.SPEED_OF_LIGHT_IN_VACUUM.getValue()) / NEWTONIAN_CONSTANT_OF_GRAVITATION.getValue(), 1.0 / 2.0), "m_P");
    public static Kelvin PLANCK_TEMPERATURE = new Kelvin(Math.pow((REDUCED_PLANCK_CONSTANT.getValue() * Math.pow(Defined.SPEED_OF_LIGHT_IN_VACUUM.getValue(), 5.0)) / NEWTONIAN_CONSTANT_OF_GRAVITATION.getValue(), 1.0 / 2.0) / Defined.BOLTZMANN_CONSTANT.getValue(), "T_P");
    public static Metre PLANCK_LENGTH = new Metre(Math.pow((REDUCED_PLANCK_CONSTANT.getValue() * NEWTONIAN_CONSTANT_OF_GRAVITATION.getValue()) / Math.pow(Defined.SPEED_OF_LIGHT_IN_VACUUM.getValue(), 3.0), 1.0 / 2.0), "l_P");
    public static Second PLANCK_TIME = new Second(Math.pow((REDUCED_PLANCK_CONSTANT.getValue() * NEWTONIAN_CONSTANT_OF_GRAVITATION.getValue()) / Math.pow(Defined.SPEED_OF_LIGHT_IN_VACUUM.getValue(), 5.0), 1.0 / 2.0), "t_P");
    public static Weber MAGNETIC_FLUX_QUANTUM = new Weber((2.0 * Math.PI * REDUCED_PLANCK_CONSTANT.getValue()) / (2.0 * Defined.ELEMENTARY_CHARGE.getValue()), "Φ_0");
    public static Siemens CONDUCTANCE_QUANTUM = new Siemens((2.0 * Math.pow(Defined.ELEMENTARY_CHARGE.getValue(), 2.0)) / (2.0 * Math.PI * REDUCED_PLANCK_CONSTANT.getValue()), "G_0");
    public static GenericDerived JOSEPHSON_CONSTANT = new GenericDerived((2.0 * Defined.ELEMENTARY_CHARGE.getValue()) / Defined.PLANCK_CONSTANT.getValue(), "K_J", "Hz V^-1");
    public static Ohm VON_KLITZING_CONSTANT = new Ohm((2.0 * Math.PI * REDUCED_PLANCK_CONSTANT.getValue()) / Math.pow(Defined.ELEMENTARY_CHARGE.getValue(), 2.0), "R_K");
    public static GenericDerived FINE_STRUCTURE_CONSTANT = new GenericDerived(Math.pow(Defined.ELEMENTARY_CHARGE.getValue(), 2.0) / (4.0 * Math.PI * VACUUM_ELECTRIC_PERMITTIVITY.getValue() * REDUCED_PLANCK_CONSTANT.getValue() * Defined.SPEED_OF_LIGHT_IN_VACUUM.getValue()), "α");
    public static GenericDerived RYDBERG_CONSTANT = new GenericDerived(10973731.568160, "R_∞", "m^-1");
    public static Joule HARTREE_ENERGY = new Joule(2.0 * Defined.PLANCK_CONSTANT.getValue() * Defined.SPEED_OF_LIGHT_IN_VACUUM.getValue() * RYDBERG_CONSTANT.getValue(), "E_h");
    public static Hertz RYDBERG_FREQUENCY = new Hertz(HARTREE_ENERGY.getValue() / (2.0 * Defined.PLANCK_CONSTANT.getValue()), "cR_∞");
    public static Kilogram ELECTRON_MASS = new Kilogram((2.0 * RYDBERG_CONSTANT.getValue() * Defined.PLANCK_CONSTANT.getValue()) / (Defined.SPEED_OF_LIGHT_IN_VACUUM.getValue() * Math.pow(FINE_STRUCTURE_CONSTANT.getValue(), 2.0)), "m_e");
    public static Kilogram PROTON_MASS = new Kilogram(1.67262192369E-27, "m_p");
    public static Kilogram NEUTRON_MASS = new Kilogram(1.67492749804E-27, "m_n");
    public static GenericDerived BOHR_MAGNETON = new GenericDerived((Defined.ELEMENTARY_CHARGE.getValue() * REDUCED_PLANCK_CONSTANT.getValue()) / (2.0 * ELECTRON_MASS.getValue()), "µ_B", "J T^-1");
    public static GenericDerived NUCLEAR_MAGNETON = new GenericDerived((Defined.ELEMENTARY_CHARGE.getValue() * REDUCED_PLANCK_CONSTANT.getValue()) / (2.0 * PROTON_MASS.getValue()), "µ_N", "J T^-1");
    public static Metre BOHR_RADIUS = new Metre((4.0 * Math.PI * VACUUM_ELECTRIC_PERMITTIVITY.getValue() * Math.pow(REDUCED_PLANCK_CONSTANT.getValue(), 2.0)) / (ELECTRON_MASS.getValue() * Math.pow(Defined.ELEMENTARY_CHARGE.getValue(), 2.0)), "a_0");
    public static GenericDerived QUANTUM_OF_CIRCULATION = new GenericDerived((Math.PI * REDUCED_PLANCK_CONSTANT.getValue()) / ELECTRON_MASS.getValue(), "πħ/m_e", "m^2 s^-1");
    public static GenericDerived FERMI_COUPLING_CONSTANT = new GenericDerived(1.1663787E-5, "G_F/(ħc)^3", "GeV^-2");
    public static GenericDerived MOLAR_PLANCK_CONSTANT = new GenericDerived(Defined.AVOGADRO_CONSTANT.getValue() * Defined.PLANCK_CONSTANT.getValue(), "N_Ah", "J Hz^-1 mol^-1");
    public static GenericDerived MOLAR_GAS_CONSTANT = new GenericDerived(Defined.AVOGADRO_CONSTANT.getValue() * Defined.BOLTZMANN_CONSTANT.getValue(), "R", "J mol^-1 K^-1");
    public static GenericDerived FARADAY_CONSTANT = new GenericDerived(Defined.AVOGADRO_CONSTANT.getValue() * Defined.ELEMENTARY_CHARGE.getValue(), "F", "C mol^-1");
    public static GenericDerived MOLAR_VOLUME_OF_IDEAL_GAS_OF_STANDARD_STATE_PRESSURE = new GenericDerived((MOLAR_GAS_CONSTANT.getValue() * 273.15) / Defined.STANDARD_STATE_PRESSURE.getValue(), "V_ssp", "m^3 mol^-1");
    public static GenericDerived LOSCHMIDT_CONSTANT_OF_STANDARD_STATE_PRESSURE = new GenericDerived(Defined.AVOGADRO_CONSTANT.getValue() / MOLAR_VOLUME_OF_IDEAL_GAS_OF_STANDARD_STATE_PRESSURE.getValue(), "n_ssp", "m^-3");
    public static GenericDerived MOLAR_VOLUME_OF_IDEAL_GAS_OF_STANDARD_ATMOSPHERE = new GenericDerived((MOLAR_GAS_CONSTANT.getValue() * 273.15) / Defined.STANDARD_ATMOSPHERE.getValue(), "V_atm", "m^3 mol^-1");
    public static GenericDerived LOSCHMIDT_CONSTANT_OF_STANDARD_ATMOSPHERE = new GenericDerived(Defined.AVOGADRO_CONSTANT.getValue() / MOLAR_VOLUME_OF_IDEAL_GAS_OF_STANDARD_ATMOSPHERE.getValue(), "n_atm", "m^-3");
    public static GenericDerived STEFAN_BOLTZMANN_CONSTANT = new GenericDerived(((Math.pow(Math.PI, 2.0) / 60.0) * Math.pow(Defined.BOLTZMANN_CONSTANT.getValue(), 4.0)) / (Math.pow(REDUCED_PLANCK_CONSTANT.getValue(), 3.0) * Math.pow(Defined.SPEED_OF_LIGHT_IN_VACUUM.getValue(), 2.0)), "σ", "W m^-2 K^-1");
}
