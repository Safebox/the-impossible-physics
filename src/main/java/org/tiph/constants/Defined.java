package org.tiph.constants;

import org.tiph.units.derived.Coulomb;
import org.tiph.units.derived.GenericDerived;
import org.tiph.units.derived.Pascal;

public class Defined
{
    public static GenericDerived SPEED_OF_LIGHT_IN_VACUUM = new GenericDerived(299792458.0, "c", "m s^-1");
    public static GenericDerived PLANCK_CONSTANT = new GenericDerived(6.62607015E-34, "h", "J Hz^-1");
    public static Coulomb ELEMENTARY_CHARGE = new Coulomb(1.602176634E-19, "e");
    public static GenericDerived AVOGADRO_CONSTANT = new GenericDerived(6.02214076E+23, "N_A", "mol^-1");
    public static GenericDerived BOLTZMANN_CONSTANT = new GenericDerived(1.380649E-23, "k", "J K^-1");
    public static Pascal STANDARD_STATE_PRESSURE = new Pascal(100000.0, "ssp");
    public static Pascal STANDARD_ATMOSPHERE = new Pascal(101325.0, "atm");
    public static GenericDerived LUMINOUS_EFFICACY = new GenericDerived(683.0, "K_cd", "lm W^-1");
}
