package org.tiph.units;

public abstract class AElectricCurrent extends AUnit
{
    protected AElectricCurrent()
    {
        super();
    }

    protected AElectricCurrent(Double value)
    {
        super(value);
    }

    protected AElectricCurrent(Double value, String symbol)
    {
        super(value, symbol);
    }
}
