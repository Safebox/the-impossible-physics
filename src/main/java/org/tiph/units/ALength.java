package org.tiph.units;

public abstract class ALength extends AUnit
{
    protected ALength()
    {
        super();
    }

    protected ALength(Double value)
    {
        super(value);
    }

    protected ALength(Double value, String symbol)
    {
        super(value, symbol);
    }
}
