package org.tiph.units;

public abstract class ALuminousIntensity extends AUnit
{
    protected ALuminousIntensity()
    {
        super();
    }

    protected ALuminousIntensity(Double value)
    {
        super(value);
    }

    protected ALuminousIntensity(Double value, String symbol)
    {
        super(value, symbol);
    }
}
