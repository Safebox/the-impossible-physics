package org.tiph.units;

public abstract class AUnit
{
    // Variables
    protected Double value;
    protected String symbol;

    // Properties
    public Double getValue()
    {
        return value;
    }

    public void setValue(Double value)
    {
        this.value = value;
    }

    public String getSymbol()
    {
        return symbol;
    }

    public void setSymbol(String symbol)
    {
        this.symbol = symbol;
    }

    //Constructors
    public AUnit()
    {
        this(null);
    }

    protected AUnit(Double value)
    {
        this(value, null);
    }

    protected AUnit(Double value, String symbol)
    {
        this.value = value;
        this.symbol = symbol;
    }
}
