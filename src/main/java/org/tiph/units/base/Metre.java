package org.tiph.units.base;

import org.tiph.units.ALength;
import org.tiph.units.non_si.AstronomicalUnit;

public class Metre extends ALength
{
    // Constructors
    public Metre()
    {
        super();
    }

    public Metre(Double value)
    {
        super(value);
    }

    public Metre(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Metre valueOf(AstronomicalUnit value)
    {
        return new Metre(value.getValue() * AstronomicalUnit.UNIT.getValue());
    }

    @Override
    public String toString()
    {
        return value.toString() +" m";
    }
}
