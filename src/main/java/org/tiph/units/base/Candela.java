package org.tiph.units.base;

import org.tiph.units.ALuminousIntensity;

public class Candela extends ALuminousIntensity
{
    // Constructors
    public Candela()
    {
        super();
    }

    public Candela(Double value)
    {
        super(value);
    }

    public Candela(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    @Override
    public String toString()
    {
        return value.toString() +" cd";
    }
}
