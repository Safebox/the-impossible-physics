package org.tiph.units.base;

import org.tiph.units.AThermodynamicTemperature;
import org.tiph.units.derived.DegreeCelsius;

public class Kelvin extends AThermodynamicTemperature
{
    // Constructors
    public Kelvin()
    {
        super();
    }

    public Kelvin(Double value)
    {
        super(value);
    }

    public Kelvin(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Kelvin valueOf(DegreeCelsius value)
    {
        return new Kelvin(value.getValue() - 273.15);
    }

    @Override
    public String toString()
    {
        return value.toString() +" K";
    }
}
