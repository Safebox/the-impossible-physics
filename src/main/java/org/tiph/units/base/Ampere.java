package org.tiph.units.base;

import org.tiph.units.AElectricCurrent;

public class Ampere extends AElectricCurrent
{
    // Constructors
    public Ampere()
    {
        super();
    }

    public Ampere(Double value)
    {
        super(value);
    }

    public Ampere(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    @Override
    public String toString()
    {
        return value.toString() +" A";
    }
}
