package org.tiph.units.base;

import org.tiph.units.ATime;

public class Second extends ATime
{
    // Constructors
    public Second()
    {
        super();
    }

    public Second(Double value)
    {
        super(value);
    }

    public Second(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Second valueOf(ATime value)
    {
        switch (value.getClass().getSimpleName())
        {
            case "Minute":
                return new Second(value.getValue() * 60);
            case "Hour":
                return new Second(value.getValue() * 3600);
            case "Day":
                return new Second(value.getValue() * 86400);
            default:
                return new Second(Double.NaN);
        }
    }

    @Override
    public String toString()
    {
        return value.toString() +" s";
    }
}
