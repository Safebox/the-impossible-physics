package org.tiph.units.base;

import org.tiph.units.AMass;
import org.tiph.units.non_si.Tonne;

public class Kilogram extends AMass
{
    // Constructors
    public Kilogram()
    {
        super();
    }

    public Kilogram(Double value)
    {
        super(value);
    }

    public Kilogram(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Kilogram valueOf(Tonne value)
    {
        return new Kilogram(value.getValue() * 1000.0);
    }

    @Override
    public String toString()
    {
        return value.toString() +" kg";
    }
}
