package org.tiph.units.base;

import org.tiph.units.AAmountOfSubstance;

public class Mole extends AAmountOfSubstance
{
    // Constructors
    public Mole()
    {
        super();
    }

    public Mole(Double value)
    {
        super(value);
    }

    public Mole(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    @Override
    public String toString()
    {
        return value.toString() +" mol";
    }
}
