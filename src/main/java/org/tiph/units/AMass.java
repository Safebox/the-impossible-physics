package org.tiph.units;

public abstract class AMass extends AUnit
{
    protected AMass()
    {
        super();
    }

    protected AMass(Double value)
    {
        super(value);
    }

    protected AMass(Double value, String symbol)
    {
        super(value, symbol);
    }
}
