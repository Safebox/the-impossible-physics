package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Second;

public class Becquerel extends AUnit
{
    // Constructors
    public Becquerel()
    {
        super();
    }

    public Becquerel(Double value)
    {
        super(value);
    }

    public Becquerel(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Becquerel valueOf(Second value)
    {
        return new Becquerel(Math.pow(value.getValue(), -1.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" Bq";
    }
}
