package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Ampere;
import org.tiph.units.base.Kilogram;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;

public class Farad extends AUnit
{
    // Constructors
    public Farad()
    {
        super();
    }

    public Farad(Double value)
    {
        super(value);
    }

    public Farad(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Farad valueOf(Kilogram kilogram, Metre metre, Second second, Ampere ampere)
    {
        return new Farad(Math.pow(kilogram.getValue(), -1.0) * Math.pow(metre.getValue(), -2.0) * Math.pow(second.getValue(), 4.0) * Math.pow(ampere.getValue(), 2.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" F";
    }
}
