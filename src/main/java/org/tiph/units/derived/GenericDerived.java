package org.tiph.units.derived;

import org.tiph.units.AUnit;

public class GenericDerived extends AUnit
{
    // Variables
    String unit;

    // Properties
    public String getUnit()
    {
        return unit;
    }

    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    // Constructors
    public GenericDerived()
    {
        this(null);
    }

    public GenericDerived(Double value)
    {
        this(value, null);
    }

    public GenericDerived(Double value, String symbol)
    {
        this(value, symbol, null);
    }

    public GenericDerived(Double value, String symbol, String unit)
    {
        super(value, symbol);
        this.unit = unit;
    }

    // Methods
    @Override
    public String toString()
    {
        return getValue().toString() + (unit != null ? " " + unit : "");
    }
}
