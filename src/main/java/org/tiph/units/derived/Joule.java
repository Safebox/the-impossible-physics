package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Kilogram;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;

public class Joule extends AUnit
{
    // Constructors
    public Joule()
    {
        super();
    }

    public Joule(Double value)
    {
        super(value);
    }

    public Joule(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Joule valueOf(Kilogram kilogram, Metre metre, Second second)
    {
        return new Joule(kilogram.getValue() * Math.pow(metre.getValue(), 2.0) * Math.pow(second.getValue(), -2.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" J";
    }
}
