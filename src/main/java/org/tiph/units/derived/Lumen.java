package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Candela;

public class Lumen extends AUnit
{
// Constructors
    public Lumen()
    {
        super();
    }

    public Lumen(Double value)
    {
        super(value);
    }

    public Lumen(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Lumen valueOf(Candela candela, Steradian steradian)
    {
        return new Lumen(candela.getValue() * steradian.getValue());
    }

    @Override
    public String toString()
    {
        return value.toString() +" lm";
    }
}
