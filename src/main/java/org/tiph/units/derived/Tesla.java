package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Ampere;
import org.tiph.units.base.Kilogram;
import org.tiph.units.base.Second;

public class Tesla extends AUnit
{
    // Constructors
    public Tesla()
    {
        super();
    }

    public Tesla(Double value)
    {
        super(value);
    }

    public Tesla(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Tesla valueOf(Kilogram kilogram, Second second, Ampere ampere)
    {
        return new Tesla(kilogram.getValue() * Math.pow(second.getValue(), -2.0) * Math.pow(ampere.getValue(), -1.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" T";
    }
}
