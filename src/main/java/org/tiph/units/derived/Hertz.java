package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Second;

public class Hertz extends AUnit
{
    // Constructors
    public Hertz()
    {
        super();
    }

    public Hertz(Double value)
    {
        super(value);
    }

    public Hertz(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Hertz valueOf(Second value)
    {
        return new Hertz(Math.pow(value.getValue(), -1.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" Hz";
    }
}
