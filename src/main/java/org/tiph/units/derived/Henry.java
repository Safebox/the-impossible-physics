package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Ampere;
import org.tiph.units.base.Kilogram;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;

public class Henry extends AUnit
{
    // Constructors
    public Henry()
    {
        super();
    }

    public Henry(Double value)
    {
        super(value);
    }

    public Henry(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Henry valueOf(Kilogram kilogram, Metre metre, Second second, Ampere ampere)
    {
        return new Henry(kilogram.getValue() * Math.pow(metre.getValue(), 2.0) * Math.pow(second.getValue(), -2.0) * Math.pow(ampere.getValue(), -2.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" H";
    }
}
