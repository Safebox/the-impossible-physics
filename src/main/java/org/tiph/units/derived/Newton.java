package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Kilogram;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;

public class Newton extends AUnit
{
    // Constructors
    public Newton()
    {
        super();
    }

    public Newton(Double value)
    {
        super(value);
    }

    public Newton(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Newton valueOf(Kilogram kilogram, Metre metre, Second second)
    {
        return new Newton(kilogram.getValue() * metre.getValue() * Math.pow(second.getValue(), -2.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" N";
    }
}
