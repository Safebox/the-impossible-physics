package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Ampere;
import org.tiph.units.base.Kilogram;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;

public class Weber extends AUnit
{
    // Constructors
    public Weber()
    {
        super();
    }

    public Weber(Double value)
    {
        super(value);
    }

    public Weber(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Weber valueOf(Kilogram kilogram, Metre metre, Second second, Ampere ampere)
    {
        return new Weber(kilogram.getValue() * Math.pow(metre.getValue(), 2.0) * Math.pow(second.getValue(), -2.0) * Math.pow(ampere.getValue(), -1.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" Wb";
    }
}
