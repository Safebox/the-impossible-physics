package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Ampere;
import org.tiph.units.base.Kilogram;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;

public class Siemens extends AUnit
{
    // Constructors
    public Siemens()
    {
        super();
    }

    public Siemens(Double value)
    {
        super(value);
    }

    public Siemens(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Siemens valueOf(Kilogram kilogram, Metre metre, Second second, Ampere ampere)
    {
        return new Siemens(Math.pow(kilogram.getValue(), -1.0) * Math.pow(metre.getValue(), -2.0) * Math.pow(second.getValue(), 3.0) * Math.pow(ampere.getValue(), 2.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" S";
    }
}
