package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Mole;
import org.tiph.units.base.Second;

public class Katal extends AUnit
{
    // Constructors
    public Katal()
    {
        super();
    }

    public Katal(Double value)
    {
        super(value);
    }

    public Katal(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Katal valueOf(Mole mole, Second second)
    {
        return new Katal(mole.getValue() * Math.pow(second.getValue(), -1.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" kat";
    }
}
