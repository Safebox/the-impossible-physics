package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Metre;

public class Steradian extends AUnit
{
    // Constructors
    public Steradian()
    {
        super();
    }

    public Steradian(Double value)
    {
        super(value);
    }

    public Steradian(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Steradian valueOf(Metre value)
    {
        return new Steradian(value.getValue());
    }

    @Override
    public String toString()
    {
        return value.toString() +" sr";
    }
}
