package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Kelvin;

public class DegreeCelsius extends AUnit
{
    // Constructors
    public DegreeCelsius()
    {
        super();
    }

    public DegreeCelsius(Double value)
    {
        super(value);
    }

    public DegreeCelsius(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static DegreeCelsius valueOf(Kelvin value)
    {
        return new DegreeCelsius(value.getValue() + 273.15);
    }

    @Override
    public String toString()
    {
        return value.toString() +" °C";
    }
}
