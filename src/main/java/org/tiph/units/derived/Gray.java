package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;

public class Gray extends AUnit
{
    // Constructors
    public Gray()
    {
        super();
    }

    public Gray(Double value)
    {
        super(value);
    }

    public Gray(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Gray valueOf(Metre metre, Second second)
    {
        return new Gray(Math.pow(metre.getValue(), 2.0) * Math.pow(second.getValue(), -2.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" Gy";
    }
}
