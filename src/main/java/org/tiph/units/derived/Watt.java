package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Kilogram;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;

public class Watt extends AUnit
{
    // Constructors
    public Watt()
    {
        super();
    }

    public Watt(Double value)
    {
        super(value);
    }

    public Watt(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Watt valueOf(Kilogram kilogram, Metre metre, Second second)
    {
        return new Watt(kilogram.getValue() * Math.pow(metre.getValue(), 2.0) * Math.pow(second.getValue(), -3.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" W";
    }
}
