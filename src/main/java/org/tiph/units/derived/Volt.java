package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Ampere;
import org.tiph.units.base.Kilogram;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;

public class Volt extends AUnit
{
    // Constructors
    public Volt()
    {
        super();
    }

    public Volt(Double value)
    {
        super(value);
    }

    public Volt(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Volt valueOf(Kilogram kilogram, Metre metre, Second second, Ampere ampere)
    {
        return new Volt(kilogram.getValue() * Math.pow(metre.getValue(), 2.0) * Math.pow(second.getValue(), -3.0) * Math.pow(ampere.getValue(), -1.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" V";
    }
}
