package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Metre;

public class Radian extends AUnit
{
    // Constructors
    public Radian()
    {
        super();
    }

    public Radian(Double value)
    {
        super(value);
    }

    public Radian(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Radian valueOf(Metre value)
    {
        return new Radian(value.getValue());
    }

    @Override
    public String toString()
    {
        return value.toString() +" rad";
    }
}
