package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;

public class Sievert extends AUnit
{
    // Constructors
    public Sievert()
    {
        super();
    }

    public Sievert(Double value)
    {
        super(value);
    }

    public Sievert(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Sievert valueOf(Metre metre, Second second)
    {
        return new Sievert(Math.pow(metre.getValue(), 2.0) * Math.pow(second.getValue(), -2.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" Sv";
    }
}
