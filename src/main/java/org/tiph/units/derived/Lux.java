package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Candela;
import org.tiph.units.base.Metre;

public class Lux extends AUnit
{
    // Constructors
    public Lux()
    {
        super();
    }

    public Lux(Double value)
    {
        super(value);
    }

    public Lux(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Lux valueOf(Candela candela, Metre metre)
    {
        return new Lux(candela.getValue() * Math.pow(metre.getValue(), -2.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" lx";
    }
}
