package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Ampere;
import org.tiph.units.base.Second;

public class Coulomb extends AUnit
{
    // Constructors
    public Coulomb()
    {
        super();
    }

    public Coulomb(Double value)
    {
        super(value);
    }

    public Coulomb(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Coulomb valueOf(Second second, Ampere ampere)
    {
        return new Coulomb(second.getValue() * ampere.getValue());
    }

    @Override
    public String toString()
    {
        return getValue().toString() + " C";
    }
}
