package org.tiph.units.derived;

import org.tiph.units.AUnit;
import org.tiph.units.base.Kilogram;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;

public class Pascal extends AUnit
{
    // Constructors
    public Pascal()
    {
        super();
    }

    public Pascal(Double value)
    {
        super(value);
    }

    public Pascal(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Pascal valueOf(Kilogram kilogram, Metre metre, Second second)
    {
        return new Pascal(kilogram.getValue() * Math.pow(metre.getValue(), -1.0) * Math.pow(second.getValue(), -2.0));
    }

    @Override
    public String toString()
    {
        return value.toString() +" Pa";
    }
}
