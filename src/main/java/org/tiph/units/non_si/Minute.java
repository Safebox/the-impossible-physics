package org.tiph.units.non_si;

import org.tiph.units.ATime;

public class Minute extends ATime
{
    // Constructors
    public Minute()
    {
        super();
    }

    public Minute(Double value)
    {
        super(value);
    }

    public Minute(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Minute valueOf(ATime value)
    {
        switch (value.getClass().getSimpleName())
        {
            case "Second":
                return new Minute(value.getValue() / 60);
            case "Hour":
                return new Minute(value.getValue() * 60);
            case "Day":
                return new Minute(value.getValue() * 1440);
            default:
                return new Minute(Double.NaN);
        }
    }

    @Override
    public String toString()
    {
        return value.toString() +" min";
    }
}
