package org.tiph.units.non_si;

import org.tiph.units.ATime;

public class Hour extends ATime
{
    // Constructors
    public Hour()
    {
        super();
    }

    public Hour(Double value)
    {
        super(value);
    }

    public Hour(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Hour valueOf(ATime value)
    {
        switch (value.getClass().getSimpleName())
        {
            case "Second":
                return new Hour(value.getValue() / 3600);
            case "Minute":
                return new Hour(value.getValue() / 60);
            case "Day":
                return new Hour(value.getValue() * 24);
            default:
                return new Hour(Double.NaN);
        }
    }

    @Override
    public String toString()
    {
        return value.toString() +" h";
    }
}
