package org.tiph.units.non_si;

import org.tiph.units.ATime;

public class Day extends ATime
{
    // Constructors
    public Day()
    {
        super();
    }

    public Day(Double value)
    {
        super(value);
    }

    public Day(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Day valueOf(ATime value)
    {
        switch (value.getClass().getSimpleName())
        {
            case "Second":
                return new Day(value.getValue() / 86400);
            case "Minute":
                return new Day(value.getValue() / 1440);
            case "Hour":
                return new Day(value.getValue() / 24);
            default:
                return new Day(Double.NaN);
        }
    }

    @Override
    public String toString()
    {
        return value.toString() +" d";
    }
}
