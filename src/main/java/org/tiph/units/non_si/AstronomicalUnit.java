package org.tiph.units.non_si;

import org.tiph.units.ALength;
import org.tiph.units.base.Metre;

public class AstronomicalUnit extends ALength
{
    // Statics
    public static final Metre UNIT = new Metre(149597870700.0);

    // Constructors
    public AstronomicalUnit()
    {
        super();
    }

    public AstronomicalUnit(Double value)
    {
        super(value);
    }

    public AstronomicalUnit(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static AstronomicalUnit valueOf(Metre value)
    {
        return new AstronomicalUnit(value.getValue() / UNIT.getValue());
    }

    @Override
    public String toString()
    {
        return value.toString() +" au";
    }
}
