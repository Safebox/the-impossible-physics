package org.tiph.units.non_si;

import org.tiph.units.AMass;
import org.tiph.units.base.Kilogram;

public class Tonne extends AMass
{
    // Constructors
    public Tonne()
    {
        super();
    }

    public Tonne(Double value)
    {
        super(value);
    }

    public Tonne(Double value, String symbol)
    {
        super(value, symbol);
    }

    // Methods
    public static Tonne valueOf(Kilogram value)
    {
        return new Tonne(value.getValue() / 1000.0);
    }

    @Override
    public String toString()
    {
        return value.toString() +" t";
    }
}
