package org.tiph.units;

public abstract class AThermodynamicTemperature extends AUnit
{
    protected AThermodynamicTemperature()
    {
        super();
    }

    protected AThermodynamicTemperature(Double value)
    {
        super(value);
    }

    protected AThermodynamicTemperature(Double value, String symbol)
    {
        super(value, symbol);
    }
}
