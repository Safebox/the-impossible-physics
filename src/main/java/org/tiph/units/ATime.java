package org.tiph.units;

public abstract class ATime extends AUnit
{
    protected ATime()
    {
        super();
    }

    protected ATime(Double value)
    {
        super(value);
    }

    protected ATime(Double value, String symbol)
    {
        super(value, symbol);
    }
}
