package org.tiph.units;

public abstract class AAmountOfSubstance extends AUnit
{
    protected AAmountOfSubstance()
    {
        super();
    }

    protected AAmountOfSubstance(Double value)
    {
        super(value);
    }

    protected AAmountOfSubstance(Double value, String symbol)
    {
        super(value, symbol);
    }
}
