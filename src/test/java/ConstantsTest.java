import org.junit.Test;
import org.tiph.constants.Defined;
import org.tiph.constants.Derived;
import org.tiph.units.AUnit;

import java.lang.reflect.Field;
import java.util.Arrays;

public class ConstantsTest
{
    @Test
    public void DisplayValues() throws IllegalAccessException
    {
        System.out.println("===Defined===");
        int n = Arrays.stream(Defined.class.getFields()).map(f ->
        {
            try
            {
                return ((AUnit) f.get(null)).getSymbol().length();
            } catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }
            return 1;
        }).mapToInt(l -> l).max().getAsInt();
        for (Field field : Defined.class.getFields())
        {
            printField(field, n);
        }

        System.out.println("===Derived===");
        n = Arrays.stream(Derived.class.getFields()).map(f ->
        {
            try
            {
                return ((AUnit) f.get(null)).getSymbol().length();
            } catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }
            return 1;
        }).mapToInt(l -> l).max().getAsInt();
        for (Field field : Derived.class.getFields())
        {
            printField(field, n);
        }
    }

    private static void printField(Field field, int n) throws IllegalAccessException
    {
        String s = "";
        int l = (n - ((AUnit) field.get(null)).getSymbol().length());
        if (l > 0)
        {
            s = String.format("%1$" + (n - ((AUnit) field.get(null)).getSymbol().length()) + "c", ' ');
        }
        System.out.println(((AUnit) field.get(null)).getSymbol() + s + " = " + field.get(null).toString());
    }
}
