import org.junit.Assert;
import org.junit.Test;
import org.tiph.units.base.Metre;
import org.tiph.units.base.Second;
import org.tiph.units.non_si.Day;
import org.tiph.units.non_si.Hour;
import org.tiph.units.non_si.Minute;

public class UnitsTest
{
    @Test
    public void Length()
    {
        Metre kilometre = new Metre(1000.0);
        Metre metre = new Metre(1.0);
        Metre centimetre = new Metre(0.01);

        System.out.println("===Length===");
        System.out.println(kilometre);
        System.out.println(metre);
        System.out.println(centimetre);

        Assert.assertEquals("1000.0 m", kilometre.toString());
        Assert.assertEquals("1.0 m", metre.toString());
        Assert.assertEquals("0.01 m", centimetre.toString());
    }

    @Test
    public void Time()
    {
        Second second = new Second(1.0);
        Minute minute = new Minute(1.0);
        Hour hour = new Hour(1.0);
        Day day = new Day(1.0);

        System.out.println("===Time===");
        System.out.println(Day.valueOf(second) + " = " + Hour.valueOf(second) + " = " + Minute.valueOf(second) + " = " + second);
        System.out.println(Day.valueOf(minute) + " = " + Hour.valueOf(minute) + " = " + minute + " = " + Second.valueOf(minute));
        System.out.println(Day.valueOf(hour) + " = " + hour + " = " + Minute.valueOf(hour) + " = " + Second.valueOf(hour));
        System.out.println(day + " = " + Hour.valueOf(day) + " = " + Minute.valueOf(day) + " = " + Second.valueOf(day));

        Assert.assertEquals("1.0 s", second.toString());
        Assert.assertEquals("1.0 min", minute.toString());
        Assert.assertEquals("1.0 h", hour.toString());
        Assert.assertEquals("1.0 d", day.toString());
    }
}
